PRODUCT_MAKEFILES := \
	$(LOCAL_DIR)/aosip_herolte.mk \
	$(LOCAL_DIR)/lineage_herolte.mk \
	$(LOCAL_DIR)/cesium_herolte.mk \
	$(LOCAL_DIR)/bootleg_herolte.mk \
	$(LOCAL_DIR)/potato_herolte.mk \
	$(LOCAL_DIR)/aosp_herolte.mk \
	$(LOCAL_DIR)/havoc_herolte.mk \
	$(LOCAL_DIR)/pixys_herolte.mk \

COMMON_LUNCH_CHOICES := \
    aosip_herolte-userdebug \
    cesium_herolte-userdebug \
    lineage_herolte-userdebug \
	bootleg_herolte-userdebug \
	potato_herolte-userdebug \
	aosp_herolte-userdebug \
	havoc_herolte-userdebug \
	pixys_herolte-userdebug \
